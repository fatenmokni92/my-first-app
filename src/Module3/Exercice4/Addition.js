import Button from "../Button"
const Addition = ({ number, setNumber, title }) => {
  return (
    <div>
      <p>
        {" "}
        {number} +2 = {number + 2}
      </p>
      <Button click={() => setNumber(number + 2)}> {title}</Button>
    </div>
  );
};
export default Addition;
