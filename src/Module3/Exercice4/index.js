import { useState } from "react";
import Multiplication from "./Multiplication";
import Addition from "./Addition";
const Math2 = () => {
  const [number, setNumber] = useState(1);
  return (
    <div>
      <Multiplication number={number} setNumber={setNumber} />
      <Addition title={"add"} number={number} setNumber={setNumber} />
    </div>
  );
};
export default Math2;
