import Button from "../Button"

const Multiplication = ({ number, setNumber}) => {
  return (
    <div>
      <p>
        {" "}
        {number} * 2 = {number * 2}{" "}
      </p>
      <Button click={() => setNumber(number * 2)}> Multiplication</Button>
    </div>
  );
};
export default Multiplication;
