// import { useState } from "react";
import { useDispatch, useSelector  } from "react-redux";
import { increment, setTitle } from "../../actions"
const CounterFunction = () => {
  // const [count, setCount] = useState(0);
  const counter = useSelector((state) => state.count )
  const title = useSelector((state) => state.title )
console.log('counter', counter)
  const dispatch = useDispatch();
  const handleClick= () => {
    // setCount(count + 1)
    dispatch(increment())
    dispatch(setTitle("Hello"))
  }
  return (
    <div>
      <p> count : {counter} </p>
      <p> {title} </p>
      <button onClick={handleClick }>Click me</button>
    </div>
  );
};
export default CounterFunction;
