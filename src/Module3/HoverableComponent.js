function HoverableComponent() {
    const changeMouseOver = (e) => {
      e.target.style.background = "green";
    };
    const changeMouseEnter = (e) => {
      e.target.style.background = "red";
    };
  
    const changeMouseLeave = (e) => {
      e.target.style.background = "white";
    };
  
    return (
      <>
        <button
          onMouseOver={changeMouseOver}
          onMouseEnter={changeMouseEnter}
          onMouseLeave={changeMouseLeave}
        >
          Hover over me!
        </button>
      </>
    );
  }
  export default HoverableComponent;