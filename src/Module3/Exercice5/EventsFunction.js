import { useState } from "react";
const EventsFunction = () => {
  const [name, setName] = useState("My button");
  const handleClick = (event, btnName) => {
    console.log("e", event);
    console.log(btnName, " clicked");
  };
  const handleChange = (event) => {
    console.log("onChange", event.target.value);
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    console.log("Button submited");
  };
  return (
    <div>
      <button key="button" onClick={(e) => handleClick(e, name)}>
        Button
      </button>{" "}
      <br />
      <form onSubmit={handleSubmit}>
        <input type="text" onChange={handleChange} />
        <button type="submit">Add Item</button>
      </form>
    </div>
  );
};
export default EventsFunction;
