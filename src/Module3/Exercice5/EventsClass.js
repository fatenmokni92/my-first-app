import React, { Component } from "react";
class EventsClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
    };
  }
  changeText(event) {
    this.setState({
      name: event.target.value,
    });
  }
  handleClick() {
    console.log("your name is:", this.state.name);
  }
  render() {
    return (
      <div>
        <label htmlFor="name">Enter your name: </label>
        <input type="text" id="name" onChange={this.changeText.bind(this)} />
        <button onClick={() => this.handleClick()}>Click me</button>
      </div>
    );
  }
}
export default EventsClass;