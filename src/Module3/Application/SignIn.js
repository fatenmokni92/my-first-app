import React, { useState } from "react";
import "./style.css";

const SignIn = () => {
  const [auth, setAuth] = useState({
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const { id, value } = e.target;
    setAuth({ ...auth, [id]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const user = JSON.parse(localStorage.getItem("authUser"));
    if (auth.email === user.email) {
      if (auth.password === user.password) {
        console.log(user.firstName + " " + user.lastName + " logged in");
      } else {
        console.log("Incorrect password");
      }
    } else {
      console.log("Incorrect email");
    }
  };

  return (
    <div className="Auth-form-container">
      <form className="Auth-form" onSubmit={handleSubmit}>
        <div className="Auth-form-content">
          <h3 className="Auth-form-title">Inscription </h3>
          <div className="form-group mt-3">
            <label>Email</label>
            <input
              type="email"
              id="email"
              className="form-control mt-1"
              placeholder="Votre Email"
              onChange={handleChange}
            />
          </div>
          <div className="form-group mt-3">
            <label>Mot de passe</label>
            <input
              type="password"
              id="password"
              className="form-control mt-1"
              placeholder="Votre Mot de passe"
              onChange={handleChange}
            />
          </div>
          <div className="d-grid gap-2 mt-3">
            <button type="submit" className="btn btn-primary">
              Enregistrer
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};
export default SignIn;
