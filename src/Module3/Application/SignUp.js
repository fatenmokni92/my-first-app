import React, { useState } from "react";
import "./style.css";

const SignUp = () => {
  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const { id, value } = e.target;
    // setUser((prevState) => ({
    //   ...prevState,
    //   [id]: value,
    // }));
    console.log('handleChange ', id, value)
    setUser({ ...user, [id]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    localStorage.setItem("authUser", JSON.stringify(user));
    
  };

  return (
    <div className="Auth-form-container">
      <form className="Auth-form" onSubmit={handleSubmit}>
        <div className="Auth-form-content">
          <h3 className="Auth-form-title">Inscription </h3>

          <div className="form-group mt-3">
            <label>Prénom</label>
            <input
              type="text"
              id="firstName"
              name="firstName"
              value={user.firstName}
              className="form-control mt-1"
              placeholder="Votre Prénom"
              onChange={handleChange}
            />
          </div>

          <div className="form-group mt-3">
            <label>Nom</label>
            <input
              type="text"
              id="lastName"
              className="form-control mt-1"
              placeholder="Votre Nom"
              onChange={handleChange}
            />
          </div>
          <div className="form-group mt-3">
            <label>Email</label>
            <input
              type="email"
              id="email"
              className="form-control mt-1"
              placeholder="Votre Email"
              onChange={handleChange}
            />
          </div>
          <div className="form-group mt-3">
            <label>Mot de passe</label>
            <input
              type="password"
              id="password"
              className="form-control mt-1"
              placeholder="Votre Mot de passe"
              onChange={handleChange}
            />
          </div>
          <div className="d-grid gap-2 mt-3">
            <button type="submit" className="btn btn-primary">
              Enregistrer
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};
export default SignUp;
