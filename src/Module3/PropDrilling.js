const WelcomeMessage = ({ name }) => {
  return <h2> Hello {name} </h2>;
};

const WelcomePage = ({ userName }) => {
  return <WelcomeMessage name={userName} />;
};

const PropDrillig = () => {
  const userName = "Mohamed";
  return (
    <>
      <WelcomePage userName={userName} />
    </>
  );
};
export default PropDrillig;
