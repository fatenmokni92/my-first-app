const WelcomeMessage = ({ name }) => {
  return <h2 style={{ color: "blue" }}> Hello {name} </h2>;
};

const WelcomePage = ({ message }) => {
  return <> {message} </>;
};

const HOC = () => {
  const userName = "Mohamed";
  return (
    <>
      <WelcomePage message={<WelcomeMessage name={userName} />} />
    </>
  );
};
export default HOC;
