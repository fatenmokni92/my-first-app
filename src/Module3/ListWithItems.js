const INITIAL_LIST = ["Button 1", "Button 2", "Button 3", "Button 4"];
const ListWithItems = () => {
  const handleClick = (event, item) => {
    console.log("e", event);
    console.log(item, "was clicked");
  };
  return (
    <div>
      {INITIAL_LIST.map((item,index) => (
        <div key={index}>
          { console.log(index, item) }
          <button key={item} onClick={(e) => handleClick(e, item)}>
            {item}
          </button>{" "}
          <br />
        </div>
      ))}
    </div>
  );
};
export default ListWithItems;