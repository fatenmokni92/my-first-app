import { Component } from "react";
class LifeCycleClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }
  componentDidMount() {
    console.log("Mount");
  }
  componentDidUpdate() {
    console.log("Update", this.state.count);
  }
  componentWillUnmount() {
    console.log("Unmount");
  }
  render() {
    return (
      <div>
        <p> count : {this.state.count} </p>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>
          Click me
        </button>
      </div>
    );
  }
}
export default LifeCycleClass;
