const comment = {
  date: "31/10/2022",
  text: "I hope you enjoy learning React!",
  author: {
    name: "Hello Kitty",
    avatarUrl: "https://placekitten.com/g/64/64",
  },
};
const Avatar = ({ author }) => {
  return <img src={author.avatarUrl} alt={author.name} />;
};
const CommentCreator = ({ user }) => {
  return (
    <>
      <Avatar author={user} />
      <div>{user.name}</div>
    </>
  );
};
const CommentData = ({ text, date }) => {
  return (
    <>
      {text} <br />
      {date}
    </>
  );
};

const CommentComposition = () => {
  return (
    <div>
      <CommentCreator user={comment.author} />
      <CommentData text={comment.text} date={comment.date} />
    </div>
  );
};

export default CommentComposition;
