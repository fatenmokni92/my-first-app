import Button from "./Button";
import CommentComposition from "./CommentComposition";
import CounterFunction from "./Execice2/CounterFunction";
import Math from "./Exercice3";
import HOC from "./HOC";
import LifeCycleClass from "./LifeCycleClass";
import LifeCycleFunction from "./LifeCycleFunction";
import PropDrillig from "./PropDrilling";
import Math2 from "./Exercice4";
import EventsFunction from "./Exercice5/EventsFunction";
import EventsClass from "./Exercice5/EventsClass";
import HoverableComponent from "./HoverableComponent"
import ListWithItems from "./ListWithItems"
const Module3 = () => {
  const onClick = () => {
    // alert("Hello");
    console.log("hello click");
  };
  return (
    <div>
      {/* <Button click={onClick}> click me </Button>
      <br />
      <br />
      <CommentComposition />
      <br />
      <br />
      <PropDrillig />
      <br />
      <br />
      <HOC /> */}
      <br />
      <br />
      {/* <CounterFunction />
      <br />
      <br />
      <LifeCycleClass/>
      <br />
      <br />
      <LifeCycleFunction/> */}
      {/* <Math />
      <br />
      <br />
      <Math2 /> */}
      <br />
      <br />
      {/* <EventsClass /> */}
      <br />
      <br />
      {/* <EventsFunction /> */}
      {/* <HoverableComponent/>  */}
      <ListWithItems />
    </div>
  );
};

export default Module3;
