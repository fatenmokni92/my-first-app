const Multiplication = ({ number }) => {
  return (
    <div>
      <p>
        {" "}
        {number} * 2 = {number * 2}{" "}
      </p>
    </div>
  );
};
export default Multiplication;
