import { useState } from "react";

import Button from "../Button";
import Multiplication from "./Multiplication"
import Addition from "./Addition";
const Math = () => {
    const [number, setNumber] = useState(1);
  return (
    <div>
      <Button 
      click={() => setNumber(number + 1)} 
      > Share 
      </Button>
      <Multiplication  number={number} />
      <Addition number={number}/>
    </div>
  );
};
export default Math;
