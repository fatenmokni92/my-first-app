import { useEffect, useState } from "react";
const LifeCycleFunction = () => {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("");

  const handleClick = () => {
    setCount(count + 1);
  };
  useEffect(() => {
    console.log("useEffect Mount");
  });

  useEffect(() => {
    console.log("useEffect Update", count);
    setName("Salah");
  }, [count]);

  useEffect(() => {}, [name]);
  console.log("name = ", name);
  useEffect(() => {
    return () => {
      console.log("useEffect Unmount", count);
    };
  }, []);

  return (
    <div>
      <p> count : {count} </p>
      <button onClick={handleClick}>Click me</button>
    </div>
  );
};
export default LifeCycleFunction;
