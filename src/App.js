import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Route, Routes, Link } from "react-router-dom";
import SignIn from "./Module3/Application/SignIn";
import SignUp from "./Module3/Application/SignUp";
import Candidature from "./Module4/Application/candidature";
import Profile from "./Module4/profile";
import NotFound from "./Module4/notFound";
import MyCandidature from "./Module4/myCandidature";
import NewCandidature from "./Module4/newCandidature";
import CounterFunction from "./Module3/Exercice2/CounterFunction";

const name = "World";
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <nav>
            <ul>
              <li>
                <Link to="/candidature">Candidature</Link>
              </li>
              <li>
                <Link to={`/candidature/new`}>New Candidature</Link>
              </li>
              <li>
                <Link to={`/candidature/myCandidature`}>My Candidature </Link>
              </li>
              <li>
                <Link to={`/profile/${name}`}>Profile</Link>
              </li>
              <li>
                <Link to="/counter">Counter</Link>
              </li>
            </ul>
          </nav>

          <Routes>
            <Route exact path="/" element={<SignIn />} />
            <Route exact path="/signup" element={<SignUp />} />
            <Route exact path="/counter" element={<CounterFunction />} />
            <Route path="/candidature">
              <Route index element={<Candidature />} />
              <Route path=":myCandidature" element={<MyCandidature />} />
              <Route path="new" element={<NewCandidature />} />
            </Route>

            <Route path="/profile/:name" element={<Profile />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
