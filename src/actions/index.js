 export const INCREMENT = 'INCREMENT'
 export const SET_TITLE = 'SET_TITLE'

export const increment=() =>{
    return{
        type: INCREMENT
    }
}

export const setTitle=(payload) =>{
    return{
        type: SET_TITLE,
        payload
    }
}