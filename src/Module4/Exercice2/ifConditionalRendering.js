const Condition1 = ({ user1, user2 }) => {
    return (
      <h5>{user1} is older than {user2} </h5>
    );
  };

const IfConditionalRendering = () => {
  const data = [
    { id: 1, name: "User1", age: 45 },
    { id: 2, name: "User2", age: 23 },
    { id: 3, name: "User2", age: 39 },
  ];

  if (data[0].age > data[1].age) {
    return <Condition1 user1={data[0].name} user2={data[1].name} />;
  } else {
    return <Condition1 user1={data[1].name} user2={data[0].name} />;
  }
};
export default IfConditionalRendering;
