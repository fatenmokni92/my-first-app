
  const TernaryOperator = () => {
    const data = [
      { id: 1, name: "User1", age: 45 },
      { id: 2, name: "User2", age: 23 },
      { id: 3, name: "User2", age: 39 },
    ];
      return (
        <div>
            <h2> Ternary operator </h2>
          {data.map((user, index) => (
            <span key={"key" + index}>
              {user.age > 30 ? (
                <li>
                  {user.name}, {user.age}
                </li>
              ) : null}
            </span>
          ))}
        </div>
      );
  
   
  };
  export default TernaryOperator;
  