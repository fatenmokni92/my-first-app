const EndOperator = () => {
  const data = [
    { id: 1, name: "User1", age: 45 },
    { id: 2, name: "User2", age: 23 },
    { id: 3, name: "User2", age: 39 },
  ];
  return (
    <div>
      <h2> logical && operator </h2>
      {data.map((user, index) => (
        <div key={"key" + index}>
          
          
              {user.name},{" "}
              <span style={{ color: "red" }}>
                {" "}
                {user.age > 40 && <span>{user.age} </span>}{" "}
              </span>
          
        </div>
      ))}
    </div>
  );
};
export default EndOperator;
