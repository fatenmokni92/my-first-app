import { useParams  } from "react-router-dom";

export default function MyCandidature(){
    const { myCandidature } = useParams();

    return(
        <h1> {myCandidature} </h1>
    )
}