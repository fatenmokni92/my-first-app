const { Component } = require("react");

class Candidature extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: "",
      age: 0,
      gender: "",
      biographie: "",
      student: false,
      email: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleBlur(event) {
    console.log("blur", event.target.value);
  }

  handleChange(event) {
    const target = event.target;
    let value = "";
    if (target.type === "checkbox") {
      value = target.checked;
    } else if (target.type === "file") {
      value = target.files[0];
    } else {
      value = target.value;
    }
    // let value =  target.type === "checkbox"
    //     ? target.checked
    //     : target.type === "file"
    //     ? target.files[0]
    //     : target.value;

    console.log("value", value);
    const name = target.name;

    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    event.preventDefault();
    localStorage.setItem("user", JSON.stringify(this.state));
  }

  render() {
    return (
      <div className="container">
        <h1>Candidature</h1>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="fullName">Entrez votre nom</label>
            <input
              type="text"
              className="form-control"
              id="fullName"
              placeholder="Nom et prénom"
              required
              name="fullName"
              value={this.state.fullName}
              onChange={this.handleChange}
              onBlur={this.handleBlur}
            />
          </div>
          <div className="form-group">
            <label htmlFor="age">Entrez votre age</label>
            <input
              type="number"
              className="form-control"
              id="age"
              placeholder="Age"
              name="age"
              value={this.state.age}
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group">
            <label htmlFor="email">Entrez votre mail</label>
            <input
              type="email"
              className="form-control"
              id="email"
              placeholder="Email"
              name="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group">
            <label htmlFor="selection">Genre</label>
            <select
              id="selection"
              className="form-control"
              name="gender"
              value={this.state.gender}
              onChange={this.handleChange}
            >
              <option value="">Liste de choix...</option>
              <option value="male">Homme</option>
              <option value="female">Femme</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="bio">Biographie</label>
            <textarea
              className="form-control"
              id="bio"
              rows="3"
              placeholder="Biographie"
              name="biographie"
              value={this.state.biographie}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="student">
              Etudiant
              <input
                name="student"
                type="checkbox"
                checked={this.state.student}
                onChange={this.handleChange}
              />
            </label>
          </div>
          <div className="form-group">
            <label htmlFor="fichier">CV</label>
            <input
              type="file"
              //   multiple
              className="form-control-file"
              id="fichier"
              name="cv"
              onChange={this.handleChange}
            />
          </div>
          <div className="d-grid gap-2 mt-3">
            <button type="submit" className="btn btn-primary">
              Enregistrer
            </button>
          </div>
        </form>
      </div>
    );
  }
}
export default Candidature;
