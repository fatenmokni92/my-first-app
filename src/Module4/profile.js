import { useParams, useNavigate, useLocation  } from "react-router-dom";
export default function Profile() {
  const { name } = useParams();
  const navigate = useNavigate();
  const { pathname } = useLocation()
  return (
    <>
      <h1>Hello {name}</h1>
      <div> Your current location is  {pathname} </div>
      {pathname ==="/profile/name" ?
      <button onClick={ () => navigate('/') } > Go home </button>
      : null}

      
    </>
  );
}

