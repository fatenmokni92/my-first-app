import { PureComponent, useState } from "react";

class Title extends PureComponent {
  render() {
    console.log("Title renders");
    return <h1>{this.props.value}</h1>;
  }
}
class Count extends PureComponent {
  render() {
    console.log("Count renders");
    return <h1>{this.props.count}</h1>;
  }
}
const CounterClass = () => {
  const [count, setCount] = useState(0);

  return (
    <div>      
      <Title value="My Title" />
      <Count count={count} />
      <button onClick={() => setCount(count + 1)}>Click me</button>      
    </div>
  );
};
export default CounterClass;