var myList = ["A", "B", "C", "D", "E"];
var numbers = [3, 8, 11, 7, 5];
const MapFunction = () => {
  const myItem = myList.map((item) => item === "C");
  console.log("myItem ", myItem);
  const myNumbers = numbers.map((num) => num * 5);
  console.log("myNumbers ", myNumbers);
  return (
    <div>
      <h2> React Map Example </h2>
      {myList.map((item) => (
        <li> {item} </li>
      ))}

      {myNumbers.map((num) => (
        <span> {num}, </span>
      ))}
    </div>
  );
};
export default MapFunction;
