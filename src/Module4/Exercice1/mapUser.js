const MapUser = () => {
  const data = [
    { id: 1, name: "User1", age: 45 },
    { id: 2, name: "User2", age: 23 },
    { id: 3, name: "User3", age: 39 },
  ];
  return (
    <div>
      <h2> User Map Example </h2>
      {data.map((user, index) => (
        <li key={index} >
        {index} . {user.name} {user.age} 
        </li>
      ))}
    </div>
  );
};
export default MapUser;
