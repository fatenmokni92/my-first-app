import { useRef } from "react";

function UncontrolledComponentFunction() {
  const nameRef = useRef();
  const emailRef = useRef();

  function onSubmit(event) {
    event.preventDefault();
    console.log("Name value: " + nameRef.current.value);
    console.log("Email value: " + emailRef.current.value);
  }
  return (
    <form onSubmit={onSubmit}>
      <label>
        Name : <input type="text" name="name" ref={nameRef} required disabled />
      </label>
      <label>
        Email : <input type="email" name="email" ref={emailRef} required />
      </label>
      <input type="submit" value="Submit" />
    </form>
  );
}
export default UncontrolledComponentFunction;
