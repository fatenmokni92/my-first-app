import { useState } from "react";

function ControlledComponentFunction() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");

  function onSubmit(event) {
    event.preventDefault();
    console.log("Name value: " + name);
    console.log("Email value: " + email);
  }
  return (
    <form onSubmit={onSubmit}>
        <h3> Controlled Component</h3>
      <label>
        Name :
        <input type="text" required name="name" value={name} onChange={(e) => setName(e.target.value)}/>
      </label>
      <label>
        Email :
        <input type="email" required name="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
      </label>
      <input type="submit" value="Submit" />
    </form>
  );
}
export default ControlledComponentFunction;


