import React from "react";
class UncontrolledComponent extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.input = React.createRef();
  }

  handleSubmit(event) {
    event.preventDefault();
    console.log("Un nom a été envoyé : " + this.input.current.value);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Nom :
          <input type="text" ref={this.input} />
        </label>
        <input type="submit" value="Envoyer" />
      </form>
    );
  }
}
export default UncontrolledComponent;
