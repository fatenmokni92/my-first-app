const PostList = ({ posts }) => (
  <div style={{ display: "flex", justifyContent: "space-around" }}>
    {posts && posts.map((post) => (
      <div className="card" style={{ width: "18rem" }} key={post.id}>
        <div className="card-body">
          <h5 className="card-title">{post.title} </h5>
          <p className="card-text">{post.body} </p>
        </div>
      </div>
    ))}
  </div>
);
export default PostList;
