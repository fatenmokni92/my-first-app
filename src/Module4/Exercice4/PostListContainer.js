import { useEffect, useState } from "react";

import data from "./data.json";
import PostList from "./PostList";

const PostListContainer = () => {
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    setPosts(data);
  }, []);
  return <PostList posts={posts} />;
};

export default PostListContainer;
