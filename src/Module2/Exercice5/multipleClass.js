import React from "react";
class MultipleClass extends React.Component {

  render() {
    return (
      <>
        <h2> Multiplication de {this.props.mycount}*2 = {this.props.mycount * 2}</h2>
      </>
    );
  }
}
export default MultipleClass;
