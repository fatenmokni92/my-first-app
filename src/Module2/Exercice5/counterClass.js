import { Component } from "react";
import Multiple from "./multipleClass";

class CounterClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      name: "",
    };
  }
  render() {
    return (
      <div>
        <p> count : {this.state.count} </p>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>
          Click me
        </button>

        <Multiple mycount={this.state.count} />
      </div>
    );
  }
}
export default CounterClass;
