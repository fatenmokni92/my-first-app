import React, { Component } from "react";
import MessageClass from "./Exercice1/messageClass";
import MessageFunction from "./Exercice1/messageFunction";
import ContentClass from "./exercice3/contentClass";
import ContentFunction from "./exercice3/contentFunction";
import CommentClass from "./Exercice4/commentClass";
import CommentFunction from "./Exercice4/commentFunction";
import CounterClass from "./Exercice5/counterClass";

const comment = {
  date: "31/10/2022",
  text: "I hope you enjoy learning React!",
  author: {
    name: "Hello Kitty",
    avatarUrl: "https://placekitten.com/g/64/64",
  },
};

console.log("my comment", comment);

class Module2 extends Component {
  render() {
    return (
      <div>
        <MessageClass /> <br />
        <MessageFunction />
        <br />
        <ContentClass message="This is a class component's props" /> <br />
        <ContentFunction message="This is a functional component's props" />
        <br />
        <CommentClass
          title="Comment in Class Component"
          date={comment.date}
          text={comment.text}
        />
        <br />
        <CommentFunction
          title="Comment in Function Component"
          comment={comment}
        />
        <CounterClass />
      </div>
    );
  }
}

export default Module2;
