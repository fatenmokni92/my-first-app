function CommentFunction(props) {
  console.log("Comment props", props);
  return (
    <div>
      <h2>{props.title}</h2>
      <div>
          <img
            src={props.comment.author.avatarUrl}
            alt={props.comment.author.name}
          />
          <div>{props.comment.author.name}</div>
        </div>
      {props.comment.text} <br />
      {props.comment.date}
      <br />
    </div>
  );
}

export default CommentFunction;
