import React, { Component } from "react";

class CommentClass extends Component {
  constructor(props) {
    super(props);
    console.log("ClassComment props", props);
  }

  render() {
    return (
      <div>
        <h2>{this.props.title}</h2>

        <div>{this.props.text}</div>
        <div>{this.props.date}</div>
      </div>
    );
  }
}

export default CommentClass;
