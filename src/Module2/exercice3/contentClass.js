import React, { Component } from "react";

class ContentClass extends Component {
  render() {
    return <h1 style={{ color: "blue" }}> {this.props.message} </h1>;
  }
}

export default ContentClass;
