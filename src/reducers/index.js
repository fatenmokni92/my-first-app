import { INCREMENT, SET_TITLE } from "../actions";
const initialState = {
  count: 0,
  title:""
};

const stateReducer = (state = initialState, action) => {
  console.log("action", action);
  let { type, payload } = action;
  switch (type) {
    case INCREMENT:
      return { ...state, count: state.count + 5 };
      case SET_TITLE:
        return { ...state, title: payload };
    default:
      return state;
  }
};
export default stateReducer;
